\documentclass{article} 

\title{Analyse Groep 4:\\Plagiaatcontrole} 
\author{Mihály Csonka\\1644219\and Ward Segers\\1642793} 
\date{22 oktober 2019}

\usepackage{graphicx}

\begin{document} 
\maketitle

\section{Algemene Aanpak}
Het doel is om plagiaat tussen geanonimiseerde Pythonprogramma's te herkennen. We veronderstellen dat elke submissie bestaat uit één bestand. De dataset omvat verschillende opdrachten met indieningen over verschillende jaren. Ook veronderstellen we dat het om zeer naïef plagiaat gaat, nl.\ identieke regels code, met mogelijk een aanpassing van namen van functies en variabelen.

Voor dit probleem maakten we het volgende plan. Aangezien de submissions zeer gelijkaardig zullen zijn, en we moeten kijken naar simpele veranderingen, is het de bedoeling om zo efficiënt en accuraat mogelijk paren van bestanden te vinden die similair zijn. We doen dit door drie stappen uit te voeren op de dataset: shingling, min-hashing, en locality-sensitive hashing.

\subsection{Shingling}
Bij onze shingling zorgen we ervoor dat we niet enkel kijken naar het aantal woorden (cfr. spatie-separated strings), maar ook naar de volgorde kijken. Bij broncode is de volgorde immers ook belangrijk voor de logica. Tevens kan shingling omgaan met het herorderen van groepen woorden.

Om onze de mogelijkheden voor shingling te bekijken, gebruiken we het volgende voorbeeld.

\begin{verbatim}def foo(array):
    sum = 0
    for value in array:
        sum += value
    return sum

foo([0, 1, 2, 3])\end{verbatim}

Omdat Pythonbestanden ASCII text bevatten, kan shingling zeer simpel worden toegepast. Dit programma levert dan bv. de volgende shingles van lengte 10: \texttt{"def foo(ar"}, \texttt{"ef foo(arr"}, \texttt{"f foo(arra"}, enz.

Een andere mogelijkheid is om gebruik te maken van de vaste structuur van Pythonprogramma's. Hierbij kunnen we dan kijken naar de verschillende soorten statements die in een Python programma kunnen voorkomen. Hierbij splitsen we de sourcecode op een newline (de statement separator van Python) en nemen van elk statement enkel het soort dat het is. Hierbij houden we geen rekening met de verdere structuur van het Pythonprogramma (we kijken bv. niet of een for-loop bij een functie hoort of niet).

Ons voorbeeld zou dan voorgesteld worden als \[\{ \texttt{DefStm}, \texttt{AssignStm}, \texttt{ForStm}, \texttt{AssignStm}, \texttt{ReturnStm}, \texttt{FnCallStm}\}\]

In het geheugen kan men deze waardes efficiënt met een enum voorstellen.

Een mogelijk nadeel aan deze tweede aanpak is dat we geen onderscheid maken tussen verschillende invullingen van deze statements. Daarnaast is de input een verzameling submissions voor een opdracht, waardoor alle programma's sowieso al heel erg gelijkaardig gaan zijn.

\section{Min-hash Implementatie}
Elk document is nu voorgesteld met een set shingles die in dat document voorkomen. Deze set is echter te groot. Om dit te krimpen tot een praktische grootte, zonder de gelijkenissen te verliezen, gebruiken we min-hashing. Dit creëert een kleinere signature voor elk bestand.

Één manier om de signatures te berekenen, is door de set shingles per bestand te representeren m.b.v.\ een bitmatrix. Hierbij zijn de rijen alle shingles en de kolommen de bestanden. Deze wordt dan \textit{n} keer willekeurig gepermuteerd om \textit{n} rijen in de signature te bekomen. De waarde van de rij voor een bestand wordt de eerste index, uit de gepermuteerde bitkolom, die 1 is als waarde heeft.

Dit is echter geen goede aanpak in de praktijk. De bitmatrix is namelijk zeer sparse en neemt veel geheugen in. Ook is het verzamelen van alle shingles over alle bestanden tijdsintensief en moeilijk in een MapReduce omgeving. Dit vergt namelijk het verzamelen van alle bestanden hun shingles op één node om de bitmatrix te kunnen construeren.

In plaats van \textit{n} permutaties, gebruiken we \textit{n} verschillende hashfuncties. Elke rij in de signature gaat dus met één van de \textit{n} hashfuncties overeenkomen. De waarde in de signature voor een bepaalde rij en bestand wordt als volgt berekend: de relevante hashfunctie hasht alle voorkomende shingles in een bestand. De minimumwaarde van deze hashresultaten is de waarde van het veld in de signature voor bijhorende hashfunctie en bestand. Op deze manier omzeilen we het probleem van ``ontbrekende data'' in de vorm van lege bitrijen. Het resultaat is echter nog steeds \textit{n} rijen waarbij de kolommen, per rij, een verband hebben. Dit verband garandeert ons dat de similaiteit tussen bestanden behouden blijft.

In de MapReduce omgeving gebruiken we de index van de hashfunctie als seed om die hashfunctie te creëeren. Dit laat ons toe om elke signature rij per bestand zonder dependencies, en dus op één individuele node, te berekenen.

\section{Pipeline Architecture}\label{pipeline}
% // key -> links in tuple
% // <T> -> list of T
% files
%1   .flat_map(|(file_name, file_content)| n * extract_shingles)     // optional hash shingles to idx
%2   .reduce(|((file_name, nth_hash), <shingles>)| {
%       rand_hash(nth_hash, shingles).then(min_hashed_shingles)
%   })
%3   .flat_map(|((file_name, nth_hash), min_hash_val)| {
%       (to_band_nr(nth_hash), (nth_hash, file_name, min_hash_val))
%   })
%4   .reduce(|(band_nr, <(nth_hash, file_name, min_hash_val)>)| {
%       order_by(nth_hash).then(each_file_to_bucket)
%   })
%5   .flat_map(|(band_nr, (bucket, <file_name>))| (bucket, <file_name>))
%6   .reduce(|(bucket, <file_name>)| check_similarity)

Om dit programma in een MapReduce-omgeving uit te voeren, hebben we de volgende pipeline uitgewerkt.

\begin{figure}
\includegraphics[width=\linewidth]{pipeline.pdf}
\caption{Visuele weergave van onze pipeline}
\end{figure}

\begin{enumerate}
    \item In de eerste map-fase extraheren we de shingles uit de sourcefiles. Elke shingle wordt \textit{n} keer emit om ze zo in \textit{n} hashfuncties te kunnen gaan gebruiken. De output key bevat de naam van de file waaruit de shingle kwam, alsook de hoeveelste emit dit is. De 'emit index' wordt als seed voor de random hashfunctie gebruikt. Dit laat ons toe elke file en hashfunctie combinatie, dus elk individueel veld in de signature matrix, zonder dependencies te berekenen.

    \item In de reduce fase hashen we de shingles per bestand en telkens voor één rij. Vervolgens zoeken we de minimumwaarde van deze hashes. De hashfunctie gebruikt de 'emit index' als seed.

    \item Hierna delen we de \textit{n} rijen op in banden.

    \item In de volgende reduce stap hashen we files per band naar buckets. Bestanden die samen komen te zitten zijn waarschijnlijk similair.
    
    \item In de laatste map kijken we welke files allemaal in dezelfde bucket zitten. Hiervan kijken we effectief hoe similar ze zijn.

    \item Tot slot reduceren we enkel de files met die boven een nader te bepalen threshold zitten. Deze bestanden zien we als plagiaat.
\end{enumerate}

\end{document}