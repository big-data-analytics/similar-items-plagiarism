#!/bin/sh

# Compile for direct runner
mvn compile -Pdirect-runner -e

# Run the bloody thing
GOOGLE_APPLICATION_CREDENTIALS=big-data-groep-4-attempt-2-afd17e807697.json mvn exec:java -Dexec.args="" -Pdirect-runner -e
