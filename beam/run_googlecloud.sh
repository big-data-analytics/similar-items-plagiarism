#!/bin/sh

mvn package -Pdataflow-runner -e

GOOGLE_APPLICATION_CREDENTIALS=big-data-groep-4-attempt-2-afd17e807697.json mvn compile exec:java \
    -Dexec.mainClass=be.uhasselt.csonka_segers.plagiarism_check.PlagiarismCheck \
    -Dexec.args="--project=big-data-groep-4-attempt-2 \
    --gcpTempLocation=gs://bda-g4-sourcedata/tmp/ \
    --runner=DataflowRunner \
    --jobName=plagiarism-check \
    --region=europe-west1 \
    --workerMachineType=n1-standard-1" \
    -Pdataflow-runner
