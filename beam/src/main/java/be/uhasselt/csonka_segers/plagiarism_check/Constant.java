package be.uhasselt.csonka_segers.plagiarism_check;

public class Constant {
    private static final String DIRECT_RUNNER_CLASS = "org.apache.beam.runners.direct.DirectRunner";

    private Constant() {
    }

    public static final String DATA_PATH(final String runner) {
        if (runner.equals(Constant.DIRECT_RUNNER_CLASS)) {
            // local
            return System.getProperty("user.dir") + "/../archive/tokens/";
        } else {
            // GCP
            return "gs://bda-g4-sourcedata/tokens/";
        }
    }

    public static final String DATA_FILE_GLOB = "*.txt";

    // TODO: play with these numbers
    public static final Integer SHINGLE_SIZE = 8;
    public static final Integer AMT_RANDOM_HASH_FN = 75;
    public static final Integer N_BUCKETS = 50000;
    public static final Integer N_BANDS = 5;
    // Setting this to 0.0 will make all the candidates pass.
    public static final Float SIMILARITY_THRESHOLD = (float) 0.90;

    public static final String OUTPUT_FILE_PATH(final String runner) {
        if (runner.equals(Constant.DIRECT_RUNNER_CLASS)) {
            return System.getProperty("user.dir") + "/output";
        } else {
            return "gs://bda-g4-sourcedata/output_" + SHINGLE_SIZE + "_" + AMT_RANDOM_HASH_FN + "_" + N_BUCKETS + "_"
                    + N_BANDS + "_" + SIMILARITY_THRESHOLD;
        }
    }
}
