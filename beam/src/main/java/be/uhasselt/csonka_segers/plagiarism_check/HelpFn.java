package be.uhasselt.csonka_segers.plagiarism_check;

import java.util.ArrayList;
import java.util.List;

public class HelpFn {
    public HelpFn() {
    }

    public static List<String> shingles_from_tokens(List<String> tokens) {
        List<String> shingles = new ArrayList<String>();
        Integer token_end_index = Constant.SHINGLE_SIZE;
        while (token_end_index < tokens.size()) {
            String shingle = tokens.subList(token_end_index - Constant.SHINGLE_SIZE, token_end_index).stream()
                    .reduce("", (a, b) -> a + b);
            shingles.add(shingle);

            token_end_index += 1;
        }

        return shingles;
    }

    // We extract the actual token. The value is unneeded as it
    // contains for example variable names. We want to check based
    // on logic.
    // TODO: are line/column numbers important?
    // Maybe we can use it to parallelise the
    // pipeline more?
    public static String token_from_line(String line) {
        return line.split("\\): ")[1].split(" '|\"")[0];
    }
}