package be.uhasselt.csonka_segers.plagiarism_check;

import be.uhasselt.csonka_segers.plagiarism_check.map.BandidxFromHashidxMapping;
import be.uhasselt.csonka_segers.plagiarism_check.map.BucketsFromBandsMapping;
import be.uhasselt.csonka_segers.plagiarism_check.map.NShinglesFromFilesMapping;
import be.uhasselt.csonka_segers.plagiarism_check.reduce.DetermineActualSimilarFilesReduction;
import be.uhasselt.csonka_segers.plagiarism_check.reduce.MinhashFromShinglesReduction;
import be.uhasselt.csonka_segers.plagiarism_check.tuple.BucketidxBandidx;
import be.uhasselt.csonka_segers.plagiarism_check.tuple.EmptySignatureCell;
import be.uhasselt.csonka_segers.plagiarism_check.tuple.FullSignatureCell;
import be.uhasselt.csonka_segers.plagiarism_check.tuple.coder.BucketidxBandidxCoder;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.CoderProvider;
import org.apache.beam.sdk.coders.CoderProviders;
import org.apache.beam.sdk.io.FileIO;
import org.apache.beam.sdk.io.FileIO.ReadableFile;
import org.apache.beam.sdk.io.FileSystems;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Distinct;
import org.apache.beam.sdk.transforms.GroupByKey;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;

public class PlagiarismCheck {

    public static void main(String[] args) {
        // Set execution options. E.g. runner used.
        final PipelineOptions options = PipelineOptionsFactory.fromArgs(args).withValidation().create();
        // Init pipeline
        final Pipeline pipeline = register_coders(Pipeline.create(options));
        // Init filesystem
        FileSystems.setDefaultPipelineOptions(options);

        final PCollection<ReadableFile> files = pipeline
                .apply("Select input files", FileIO.match()
                        .filepattern(Constant.DATA_PATH(options.getRunner().getName())
                                + Constant.DATA_FILE_GLOB))
                .apply("Read input files", FileIO.readMatches());

        final PCollection<KV<EmptySignatureCell, Integer>> min_hashes = files
                .apply("Emit shingles * n", ParDo.of(new NShinglesFromFilesMapping()))
                .apply("Minhashing", ParDo.of(new MinhashFromShinglesReduction()));

        final PCollection<KV<Integer, Iterable<FullSignatureCell>>> banded_files = min_hashes
                .apply("Band number", ParDo.of(new BandidxFromHashidxMapping()))
                .apply("Group by band number", GroupByKey.<Integer, FullSignatureCell>create());

        final PCollection<String> actual_similar_files = banded_files
                .apply("Bucket files", ParDo.of(new BucketsFromBandsMapping()))
                .apply("Group by (bucket, band)", GroupByKey.<BucketidxBandidx, String>create())
                .apply("Compare candidates", ParDo.of(new DetermineActualSimilarFilesReduction(
                        options.getRunner().getName())));

        actual_similar_files.apply(Distinct.<String>create()) //
                .apply("Log flagged files", TextIO.write()
                        .to(Constant.OUTPUT_FILE_PATH(options.getRunner().getName())));

        pipeline.run().waitUntilFinish();
    }

    // Custom coders for the tuple types used as keys are required. This is due
    // to SerializedCoder being non-deterministic. I.e. serialized keys might
    // not match during the grouping phase.
    public static Pipeline register_coders(final Pipeline pipeline) {
        CoderProvider provider = CoderProviders.fromStaticMethods(BucketidxBandidx.class,
                BucketidxBandidxCoder.class);
        pipeline.getCoderRegistry().registerCoderProvider(provider);
        return pipeline;
    }
}
