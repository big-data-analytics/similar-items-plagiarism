package be.uhasselt.csonka_segers.plagiarism_check.map;

import be.uhasselt.csonka_segers.plagiarism_check.Constant;
import be.uhasselt.csonka_segers.plagiarism_check.tuple.EmptySignatureCell;
import be.uhasselt.csonka_segers.plagiarism_check.tuple.FullSignatureCell;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;

public class BandidxFromHashidxMapping extends DoFn<KV<EmptySignatureCell, Integer>, KV<Integer, FullSignatureCell>> {
    @ProcessElement
    public void processElement(final ProcessContext context) {
        final Integer band_size = Constant.AMT_RANDOM_HASH_FN / Constant.N_BANDS;
        final Integer band_nr = context.element().getKey().hash_index / band_size;
        context.output(KV.of(band_nr, new FullSignatureCell(context.element().getKey().hash_index,
                context.element().getKey().filename, context.element().getValue())));
    }
}
