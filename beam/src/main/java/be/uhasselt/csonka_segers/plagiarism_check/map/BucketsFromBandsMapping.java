package be.uhasselt.csonka_segers.plagiarism_check.map;

import be.uhasselt.csonka_segers.plagiarism_check.Constant;
import be.uhasselt.csonka_segers.plagiarism_check.tuple.BucketidxBandidx;
import be.uhasselt.csonka_segers.plagiarism_check.tuple.FullSignatureCell;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;

/**
 * Reduces items into their bands.
 */
public class BucketsFromBandsMapping
        extends DoFn<KV<Integer, Iterable<FullSignatureCell>>, KV<BucketidxBandidx, String>> {
    @ProcessElement
    public void processElement(final ProcessContext context) {
        Set<String> all_filenames = StreamSupport.stream(context.element().getValue().spliterator(), false)
                .map(data -> data.filename).collect(Collectors.toSet());

        all_filenames.iterator().forEachRemaining(filename -> {
            Stream<FullSignatureCell> file_band = StreamSupport
                    .stream(context.element().getValue().spliterator(), false)
                    .filter(data -> data.filename.equals(filename));
            Integer bucket = bucket_from_band(file_band.map(data -> data.min_hash));

            context.output(KV.of(new BucketidxBandidx(bucket, context.element().getKey()), filename));
        });
    }

    // This function hashes a file band to a bucket.
    private Integer bucket_from_band(Stream<Integer> file_band) {
        return file_band.mapToInt(s -> s).sum() % Constant.N_BUCKETS;
    }
}
