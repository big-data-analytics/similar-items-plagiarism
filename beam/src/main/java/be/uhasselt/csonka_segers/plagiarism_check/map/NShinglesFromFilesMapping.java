package be.uhasselt.csonka_segers.plagiarism_check.map;

import be.uhasselt.csonka_segers.plagiarism_check.Constant;
import be.uhasselt.csonka_segers.plagiarism_check.HelpFn;
import be.uhasselt.csonka_segers.plagiarism_check.tuple.EmptySignatureCell;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.List;
import org.apache.beam.sdk.io.FileIO.ReadableFile;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;

public class NShinglesFromFilesMapping extends DoFn<ReadableFile, KV<EmptySignatureCell, Iterable<String>>> {
    @ProcessElement
    public void processElement(final ProcessContext context) throws IOException {
        final ReadableFile file = context.element();
        final String filename = file.getMetadata().resourceId().getFilename();
        BufferedReader reader = new BufferedReader(new InputStreamReader(Channels.newInputStream(file.open())));

        List<String> tokens = new ArrayList<String>();
        String line = null;
        while ((line = reader.readLine()) != null) {
            if (!line.equals("")) {
                tokens.add(HelpFn.token_from_line(line));
            }
        }

        List<String> shingles = HelpFn.shingles_from_tokens(tokens);

        // A document with a size smaller than the shingle size will not be looked at.
        // It makes no sense to try to compare this document as we don't have enough
        // information to analyze it.
        if (!shingles.isEmpty()) {
            for (Integer n = 0; n < Constant.AMT_RANDOM_HASH_FN; n++) {
                context.output(KV.of(new EmptySignatureCell(n, filename), shingles));
            }
        }
    }
}