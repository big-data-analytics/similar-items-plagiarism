package be.uhasselt.csonka_segers.plagiarism_check.reduce;

import be.uhasselt.csonka_segers.plagiarism_check.Constant;
import be.uhasselt.csonka_segers.plagiarism_check.HelpFn;
import be.uhasselt.csonka_segers.plagiarism_check.tuple.BucketidxBandidx;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.vendor.guava.v26_0_jre.com.google.common.collect.Lists;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import org.apache.beam.sdk.io.FileSystems;
import org.apache.beam.sdk.io.fs.ResourceId;

public class DetermineActualSimilarFilesReduction //
        extends DoFn<KV<BucketidxBandidx, Iterable<String>>, String> {
    final String runner;

    public DetermineActualSimilarFilesReduction(final String runner) {
        this.runner = runner;
    }

    @ProcessElement
    public void processElement(final ProcessContext context) throws IOException {
        final List<String> filenames = Lists.newArrayList(context.element().getValue());

        for (int i = 0; i < filenames.size(); i++) {
            for (int j = 0; j < filenames.size(); j++) {
                // Prevent duplicate (swapped) combinations and combinations of
                // the same file.
                if (i < j) {
                    String filename1 = filenames.get(i);
                    String filename2 = filenames.get(j);
                    List<String> shingles_file1 = read_shingles_from_file(filename1);
                    List<String> shingles_file2 = read_shingles_from_file(filename2);
                    if (Float.compare(jaccard_similarity(shingles_file1, shingles_file2),
                            Constant.SIMILARITY_THRESHOLD) > 0) {
                        context.output(filename1.split("\\.")[0] + ".py" + ", " //
                                + filename2.split("\\.")[0] + ".py");
                    }
                }
            }
        }
    }

    private List<String> read_shingles_from_file(String filename) throws IOException {
        List<String> tokens = new ArrayList<String>();

        ResourceId ri = FileSystems.matchNewResource(Constant.DATA_PATH(this.runner) + filename, false);
        ReadableByteChannel f = FileSystems.open(ri);

        BufferedReader reader = new BufferedReader(new InputStreamReader(Channels.newInputStream(f)));

        String line = null;
        while ((line = reader.readLine()) != null) {
            if (!line.equals("")) {
                tokens.add(HelpFn.token_from_line(line));
            }
        }

        return HelpFn.shingles_from_tokens(tokens);
    }

    // Calculates the actual similarity between 2 files.
    private Float jaccard_similarity(final List<String> file1, final List<String> file2) {
        // We decide to count duplicate items individually. E.g. the token ``[``
        // at index 0 is different from the token ``[`` at index 1.
        Set<String> set1 = tag_individual_tokens(file1);
        Set<String> set2 = tag_individual_tokens(file2);

        return Float.valueOf(intersection(set1, set2).size()) //
                / Float.valueOf(union(set1, set2).size());
    }

    private Set<String> tag_individual_tokens(final List<String> tokens) {
        HashMap<String, Integer> seen_tokens = new HashMap<String, Integer>();
        Set<String> token_set = new HashSet<String>();

        for (String token : tokens) {
            if (seen_tokens.containsKey(token)) {
                Integer times_seen = seen_tokens.get(token);
                seen_tokens.put(token, times_seen + 1);
                token_set.add(new String(token + (times_seen + 1)));
            } else {
                seen_tokens.put(token, 0);
                token_set.add(new String(token + 0));
            }
        }

        return token_set;
    }

    private Set<String> intersection(Set<String> set1, Set<String> set2) {
        set1.retainAll(set2);
        return set1;
    }

    private Set<String> union(Set<String> set1, Set<String> set2) {
        set1.addAll(set2);
        return set1;
    }
}
