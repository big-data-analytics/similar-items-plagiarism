package be.uhasselt.csonka_segers.plagiarism_check.reduce;

import be.uhasselt.csonka_segers.plagiarism_check.exception.NoMinimumValueDeducible;
import be.uhasselt.csonka_segers.plagiarism_check.tuple.EmptySignatureCell;
import java.math.BigInteger;
import java.util.stream.StreamSupport;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.values.KV;
import org.apache.commons.codec.digest.DigestUtils;

public class MinhashFromShinglesReduction
    extends DoFn<KV<EmptySignatureCell, Iterable<String>>,
                 KV<EmptySignatureCell, Integer>>
{
    @ProcessElement
    public void processElement(final ProcessContext context)
        throws NoMinimumValueDeducible
    {
        final Integer hash_index        = context.element().getKey().hash_index;
        final Iterable<String> shingles = context.element().getValue();

        Integer minimum_hash
            = StreamSupport.stream(shingles.spliterator(), false)
                  .map(shingle -> random_hash_fn(shingle, hash_index))    //
                  .mapToInt(i -> i)
                  .min()    //
                  .orElseThrow(() -> new NoMinimumValueDeducible());

        context.output(KV.of(context.element().getKey(), minimum_hash));
    }

    // This is all random hash function in 1 place. The provided seed
    // differentiates each hash function from each other. I.e. passing a
    // different seed creates a different random hash function.
    private Integer random_hash_fn(final String shingle, final Integer seed)
    {
        // We want almost no collision since we're simulating the scrambling of
        // rows. Thus the modulation term is chosen as big as possible.
        BigInteger hashed_shingle
            = new BigInteger(DigestUtils.md5Hex(shingle), 16);

        return (hashed_shingle.add(BigInteger.valueOf(seed)))
            .mod(BigInteger.valueOf(Integer.MAX_VALUE))
            .intValue();
    }
}