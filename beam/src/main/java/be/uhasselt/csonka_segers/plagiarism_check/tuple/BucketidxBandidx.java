package be.uhasselt.csonka_segers.plagiarism_check.tuple;

import java.io.Serializable;

public class BucketidxBandidx implements Serializable
{
    public Integer bucket_idx;
    public Integer band_idx;

    public BucketidxBandidx(final Integer bucket_idx, final Integer band_idx)
    {
        this.bucket_idx = bucket_idx;
        this.band_idx   = band_idx;
    }

    public boolean equals(BucketidxBandidx other)
    {
        return this.bucket_idx.equals(other.bucket_idx)    //
            && this.band_idx.equals(other.band_idx);
    }
}