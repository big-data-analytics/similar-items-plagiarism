package be.uhasselt.csonka_segers.plagiarism_check.tuple;

import java.io.Serializable;

public class EmptySignatureCell implements Serializable {
    public final Integer hash_index;
    public final String filename;

    public EmptySignatureCell(final Integer hash_index, final String filename) {
        this.hash_index = hash_index;
        this.filename = filename;
    }

    public boolean equals(Object other) {
        EmptySignatureCell casted_other = (EmptySignatureCell) other;
        return this.hash_index.equals(casted_other.hash_index) //
                && this.filename.equals(casted_other.filename);
    }
}
