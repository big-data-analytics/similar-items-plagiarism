package be.uhasselt.csonka_segers.plagiarism_check.tuple;

public class FullSignatureCell extends EmptySignatureCell {
    public Integer min_hash;

    public FullSignatureCell(final Integer hash_index, final String filename, final Integer min_hash) {
        super(hash_index, filename);
        this.min_hash = min_hash;
    }

    @Override
    public boolean equals(Object other) {
        FullSignatureCell casted_other = (FullSignatureCell) other;
        return this.hash_index.equals(casted_other.hash_index) //
                && this.min_hash.equals(casted_other.min_hash) //
                && this.filename.equals(casted_other.filename);
    }
}