package be.uhasselt.csonka_segers.plagiarism_check.tuple.coder;

import be.uhasselt.csonka_segers.plagiarism_check.tuple.BucketidxBandidx;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.beam.sdk.coders.CoderException;
import org.apache.beam.sdk.coders.CustomCoder;
import org.apache.beam.sdk.coders.VarIntCoder;

public class BucketidxBandidxCoder extends CustomCoder<BucketidxBandidx>
{
    public static BucketidxBandidxCoder of()
    {
        return new BucketidxBandidxCoder();
    }

    @Override
    public void encode(BucketidxBandidx item, OutputStream output)
        throws CoderException, IOException
    {
        VarIntCoder int_coder = VarIntCoder.of();

        int_coder.encode(item.bucket_idx, output);
        int_coder.encode(item.band_idx, output);
    }

    @Override
    public BucketidxBandidx decode(InputStream input)
        throws CoderException, IOException
    {
        VarIntCoder int_coder = VarIntCoder.of();

        BucketidxBandidx tuple = new BucketidxBandidx(0, 0);
        tuple.bucket_idx       = int_coder.decode(input);
        tuple.band_idx         = int_coder.decode(input);

        return tuple;
    }

    @Override
    public void verifyDeterministic() throws NonDeterministicException
    {
    }
}